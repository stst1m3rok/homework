"use strict"

function createNewUser(firstName, lastName) {
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin: function() {
            return this.firstName.charAt(0).toLowerCase()
                + this.lastName.toLowerCase();
        },
    }
    return newUser;
}


let newUser = createNewUser(prompt('Enter your name'), prompt('Enter your last name'))

console.log(newUser.getLogin());

