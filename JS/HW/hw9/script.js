'use strict'
window.onload = () => {
    const arrayList = (array, target = document.querySelector('body')) => {
        let ul = document.createElement('ul')
        array.forEach(elem => {
            let li = document.createElement('li')
            if (Array.isArray(elem)) {
                ul.appendChild(arrayList(elem, li))
            } else {
                li.textContent = elem;
                ul.appendChild(li);
            }
        })

        target.appendChild(ul)
        return target;
    }

    let array = ['hello', 'world', ['Kyiv', 'Kharkiv', [1, 2, 3]], 'Odessa', 'Lviv'];
    arrayList(array);
}
