'use strict'
window.onload = () => {
    let imagesCollection = document.querySelectorAll('div.images-wrapper > img.image-to-show');
    for (let i = 1; i < imagesCollection.length; i++) {
        imagesCollection[i].classList.add('hidden');
    }
    let currentInterval;
    let imagesSwitcher = () => {
        let images = document.querySelectorAll('div.images-wrapper > img.image-to-show');
        for (let image of images) {
            if (!image.classList.contains('hidden')) {
                image.classList.add('hidden')
                if (image.nextElementSibling) {
                    image.nextElementSibling.classList.remove('hidden')
                } else {
                    images[0].nextElementSibling.classList.remove('hidden')
                }
                break
            }
        }
    }


    function startImagesSwitcher() {
        currentInterval = setInterval(() => {
            imagesSwitcher()
        }, 3000);
    }

    document.querySelector('#btn_stop')
        .addEventListener('click', () => {
            clearInterval(currentInterval)
        })
    document.querySelector('#btn_start')
        .addEventListener('click', () => {
            clearInterval(currentInterval)
            startImagesSwitcher()
        })

    startImagesSwitcher()
}