'use strict'
window.onload = () => {
    let currentTheme = localStorage.getItem('theme')
    if (currentTheme) {
        changeTheme(currentTheme)
    } else {
        currentTheme = 'day'
        localStorage.setItem('theme', currentTheme)
    }

    console.log(localStorage.getItem('theme'))

    document.getElementById('theme_button')
        .addEventListener('click', () => {
            if (currentTheme == 'day') {
                currentTheme = 'night'
            } else if (currentTheme == 'night') {
                currentTheme = 'day'
            }
            localStorage.setItem('theme', currentTheme);
            changeTheme(currentTheme)
        })

    function changeTheme(theme) {

        if (theme == 'night') {
            document.querySelector('.page-wrapper').style.filter = 'invert(100)'
            document.querySelector('img').style.filter = 'invert(100)'
            document.querySelector('.logo').style.filter = 'invert(100)'
        } else if (theme == 'day') {
            document.querySelector('.page-wrapper').style.filter = 'invert(0)'
            document.querySelector('img').style.filter = 'invert(0)'
            document.querySelector('.logo').style.filter = 'invert(0)'
        }
    }

}

