function createNewUser(firstName, lastName, birthday) {
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        getLogin: function () {
            return this.firstName.charAt(0).toLowerCase()
                + this.lastName.toLowerCase();
        },
        getAge: function () {
            let age = ((new Date().getTime() -
                        new Date(this.birthday.split('.').reverse().join('.'))) /
                        (24 * 3600 * 365.25 * 1000)) | 0;
            return age;
        },
        getPass: function () {
            let password = this.firstName.charAt(0).toUpperCase()
                + this.lastName.toLowerCase() + this.birthday.split('.').slice(-1);
            return password;
        },

    }
    return newUser;
}


let newUser = createNewUser(prompt('Enter your name'), prompt('Enter your last name'), prompt('Enter your birthdate'))

console.log(newUser);
console.log(newUser.getAge(), newUser.getPass());


