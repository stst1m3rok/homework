window.onload = () => {
    let buttonArray = document.querySelectorAll('.btn');
    let currentButton = document.querySelector('.btn');
    document.addEventListener('keydown', ev => {
        for (let button of buttonArray) {
            if (button.textContent.toLowerCase() == ev.key.toLowerCase()) {
                currentButton.style.backgroundColor = 'black';
                button.style.backgroundColor = 'blue';
                currentButton = button;
                break;
            }
        }
    })
}