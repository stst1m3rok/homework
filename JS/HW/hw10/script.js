window.onload = () => {
    showTabsContent(document.querySelector('.tabs-title.active').textContent);

    document.querySelectorAll('.tabs-title').forEach(tab => {
        tab.addEventListener('click', (evt => {
            document.querySelectorAll('.tabs-title')
                .forEach(tab => tab.classList.remove('active'));
            evt.target.classList.add('active');
            showTabsContent(evt.target.textContent);
        }))
    })

    function showTabsContent(tabTitle) {
        document.querySelectorAll('.tabs-content > li')
            .forEach(li => {
                if (li.dataset.name != tabTitle) {
                    li.style.display = 'none';
                } else {
                    li.style.display = 'initial';
                }
            })
    }
}

