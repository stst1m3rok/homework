window.onload = () => {
    let icons = document.querySelectorAll('i.icon-password')

    toggleIconsVisibility('fa-eye')
    icons.forEach(icon => {
        icon.addEventListener('click', (evt => {
            if (evt.target.classList.contains('fa-eye')) {
                toggleIconsVisibility('fa-eye-slash')
                document.querySelectorAll('.input-wrapper > input[type=password]')
                    .forEach(input => {
                        input.setAttribute('type', 'text')
                    })
            } else if (evt.target.classList.contains('fa-eye-slash')) {
                toggleIconsVisibility('fa-eye')
                document.querySelectorAll('.input-wrapper > input[type=text]')
                    .forEach(input => {
                        input.setAttribute('type', 'password')
                    })
            }
        }))
    })

    function toggleIconsVisibility(iconClassToShow) {
        icons.forEach(icon => {
            if (!icon.classList.contains(iconClassToShow)) {
                icon.style.display = 'none';
            } else {
                icon.style.display = 'initial';
            }
        })
    }

    document.querySelector('.password-form > button[type=submit]')
        .addEventListener('click', evt => {
            evt.preventDefault();
            if (document.getElementById('pass1').value === document.getElementById('pass2').value) {
                document.querySelector('.password-form').classList.remove('error')
                setTimeout(() => {
                             alert('You are welcome!')
                }, 200)

            } else {
                document.querySelector('.password-form').classList.add('error')

            }
        })

}